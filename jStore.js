/**
 * jStore
 * checkbox status as JSON data to local storage
 * Copyright 2019
 * Authors: Mika Tervonen, <mtervonen80@gmail.com>
 * All Rights Reserved.
 * Use, reproduction, distribution, and modification of this code is subject to the terms and
 * conditions of the MIT license, available at http://www.opensource.org/licenses/mit-license.php
 *
 */
/* global define */
(function(define) {
    define(["jquery"], function($) {
        return (function() {

            const storage = "jStore";

            var jStore = {
                getItem: getItem,
                getAll: getAll,
                watch: watch,
            };

            return jStore;

            function getAll() {
                let data = JSON.parse(localStorage.getItem(storage));
                if (data) {
                    return data[0];
                }
                return;
            }

            function setAll() {
                let items = [{}];
                let list = $(".jStore");

                if (!list[0]) {
                    console.error("jStore can't find any elements!");
                } else {
                    for (let item of list) {
                        items[0][item.id] = item.checked;
                    }
                    return localStorage.setItem(storage, JSON.stringify(items));
                }
            }

            function setItem(key, value) {
                let data = JSON.parse(localStorage.getItem(storage));
                data[0][key] = value;
                return localStorage.setItem(storage, JSON.stringify(data));
            }

            function getItem(key) {
                let data = JSON.parse(localStorage.getItem(storage));
                return data[0][key];

            }

            function handleEvents() {
                $(".jStore:checkbox").on("change", function(e) {
                    const el = this,
                        checked = el.checked;
                    let id = $(this).attr("id");

                    if (!checked) {
                        setItem(id, false);
                    } else {
                        setItem(id, true);
                    }
                });
            }

            function setDefaults() {
                $.each(getAll(), function(key, value) {
                    if (value) {
                        $("#" + key).prop("checked", true);
                    } else {
                        $("#" + key).prop("checked", false);
                    }
                });
            }

            function watch() {
                if (!getAll()) {
                    setAll()
                }
                setDefaults();
                return handleEvents();
            }

        })();
    });
}(typeof define === "function" && define.amd ? define : function(deps, factory) {
    window.jStore = factory(window.jQuery);
}));
